
/**
 *  innehåller fördefinierade fonter och färger
**/


/** förhindrar att Titanium formaterar om koden här under **/
//formatter:off


/**
 ** gradients
*/

//hur man skapar en gradient för att tex använda i en view
var barGradient = {
	type: 'linear',
	startPoint: { x: '0%', y: '0%' 	 },
	endPoint: 	{ x: '0%', y: '100%' },
	colors: [
		{ color: '#ffffff', offset: 0.0 },
		{ color: '#a0a0a0', offset: 1.0 }
	]
};


/**
 * 	colors
**/
var blackFontColor = '#222',
	whiteFontColor = '#eee',

	//färggranna
	greenFontColor = '#60c104',
	orangeColor = '#f2a11f',
	turkouiseFontColor = '#21b9d2',
	blueFontColor = '#4EB1FD',

	//special
	winBgColor = '#fff',
	navbarColor = '#ed1b2f',
	navbarTitleFontColor = '#fff',

	//grå-aktiga
	darkGrayColor = '#787878',
	grayColor = '#5b5b5b',
	lightGrayColor = '#b1b1b1',


	//border & lines
	dividerLineColor = '#c7c7c7';


/**
 ** fonts (här kan man ange egna fonts)
*/
if (OS_is_Android) {

	// on Android, use the "base name" of the file (file name without extension)
	var defFam = 'Roboto';
	var defFamBold = 'Roboto Bold';

}else{
	// use the friendly-name on iOS
	var defFam = 'Copperplate';
	var defFamBold = 'Copperplate';
}

var fonts = [];
for (var i = 10; i < 48; i++) {
	fonts[i] = [];
	fonts[i]['normal'] = 	{	fontFamily: defFam,			fontSize: i+'sp',	fontWeight: 'normal',	fontStyle: 'normal' };
	fonts[i]['bold'] =	 	{	fontFamily: defFamBold,		fontSize: i+'sp',	fontWeight: 'bold',		fontStyle: 'normal' };
}


//formatter:on

