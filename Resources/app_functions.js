
var OS_is_Android = Ti.Platform.osname == "android";
var OS_is_iOS = !OS_is_Android;

var OS_is_iOS6 = false,
	OS_is_iOS7 = false,
	OS_is_iOS7Plus = false,
	OS_is_iOS8 = false;
if (OS_is_iOS){
	var ver = parseInt(Ti.Platform.version, 10);
	OS_is_iOS6 = ver >= 6 && ver < 7;
	OS_is_iOS7 = ver >= 7 && ver < 8;
	OS_is_iOS7Plus = ver >= 7;
	OS_is_iOS8 = ver >= 8 && ver < 9;
}

var device_is_iPad = Ti.Platform.osname === 'ipad';
var device_is_Phone = Ti.Platform.osname !== 'ipad';
var device_is_Simulator = ((Ti.Platform.model.toUpperCase() == "SIMULATOR") || (Ti.Platform.model.toUpperCase() == "GOOGLE_SDK"));

var isRetina = Ti.Platform.displayCaps.density == 'high';
var is_35Inch = Ti.Platform.displayCaps.platformHeight < 568;
var is_at3x =  Ti.Platform.displayCaps.dpi >= 480; //iPhone 6 Plus


if (OS_is_Android){
	var androidSoftInput = Ti.UI.Android.SOFT_INPUT_STATE_ALWAYS_HIDDEN | Ti.UI.Android.SOFT_INPUT_ADJUST_PAN;
}
var ScreenIsLowDpi = Ti.Platform.displayCaps.density == 'low';

var debugMode = true;//!! TODO: bör vara false i släppt version!


Ti.API.info('***********************************************************');
Ti.API.info('Ti.Platform.osname        = '+Ti.Platform.osname);
Ti.API.info('Ti.Platform.version       = '+Ti.Platform.version);
Ti.API.info('Ti.Platform.model         = '+Ti.Platform.model.toUpperCase());
Ti.API.info(' ');
Ti.API.info('  >> device_is_Simulator  = '+device_is_Simulator);
Ti.API.info('  >> device_is_Phone      = '+device_is_Phone);
Ti.API.info('  >> isRetina             = '+isRetina);
Ti.API.info('  >> is_35Inch            = '+is_35Inch);
Ti.API.info('  >> is_at3x              = '+is_at3x);
Ti.API.info(' ');
Ti.API.info('***********************************************************');


Ti.include('fonts_and_colors.js');


/**
 * 	animeringar
 */
var fadeInAnim = Ti.UI.createAnimation({
	duration: 180,
	opacity: 1.0
});

var fadeTo50 = Ti.UI.createAnimation({
	duration: 180,
	opacity: 0.5
});

var fadeOutAnim = Ti.UI.createAnimation({
	duration: 180,
	opacity: 0.0
});

var showWinTabStyleAnim = Ti.UI.createAnimation({
	left: 0,
	duration: 350
});

var hideWinTabStyleAnim = Ti.UI.createAnimation({
	left: Ti.Platform.displayCaps.platformWidth,
	duration: 300
});

var showWinModalStyleAnim = Ti.UI.createAnimation({
	top: OS_is_iOS7Plus ? 0 : -20,
	duration: 350
});

var hideWinModalStyleAnim = Ti.UI.createAnimation({
	top: Ti.Platform.displayCaps.platformHeight,
	duration: 300
});



/**
 * 	en "laddar" (loading) view
 */
var loadingView, activityIndicator;

function showLoading() {
	logg('showLoading()');

	if (loadingView === undefined) {
		logg('creating loadingView...');

		loadingView = Ti.UI.createView({
			top: 0,
			height: '100%',
			width: '100%',
			opacity: OS_is_iOS ? 0.0 : 1.0,
			zIndex: 99999999,
			visible: OS_is_Android ? false : true
		});
		win.add(loadingView);

		if (OS_is_iOS){
			var loadingBgView = Ti.UI.createView({
				height: '100%',
				width: '100%',
				backgroundGradient: {
			        type: 'radial',
			        startPoint: { x: '50%', y: '50%' },
			        endPoint: { x: '50%', y: '50%' },
					colors: [{ color: 'transparent', offset: 0.0}, { color: '#000', offset: 0.85 }],
			        startRadius: '0%',
			        endRadius: '100%',
			        backfillStart: true,
			        backfillEnd: true
			    },
				opacity: 0.5
			});
		}else{
			var loadingBgView = Ti.UI.createView({
				height: '100%',
				width: '100%',
				backgroundColor: '#000',
				opacity: 0.5
			});
		}
		loadingView.add(loadingBgView);

		activityIndicator = Ti.UI.createActivityIndicator({
			style: OS_is_iOS ? Titanium.UI.iPhone.ActivityIndicatorStyle.DARK : Titanium.UI.ActivityIndicatorStyle.DARK
		});
		loadingView.add(activityIndicator);

	}
	if (OS_is_Android){
		loadingView.show();
	}else{
		loadingView.animate(fadeInAnim);
	}
	activityIndicator.show();
}

function hideLoading(animateIt) {
	if (loadingView !== undefined) {
		activityIndicator.hide();
		if (OS_is_Android){
			loadingView.hide();
			logg('Android - hiding loadingview...');
		}else{
			if (animateIt) {
				setTimeout(function(e) {
					loadingView.animate(fadeOutAnim);
					setTimeout(function(e) {
						loadingView.top = 0;
						// loadingView.opacity = 1.0;
					}, fadeOutAnim.duration + 25);
				}, fadeInAnim.duration + 25);
			} else {
				loadingView.opacity = 0.0;
			}
		}
	}
}


/**
 * 	"string" funktioner
 */

//används av charPadding
function generateString(str, noOfRepeats) {
   	noOfRepeats = noOfRepeats || 1;
    return Array(noOfRepeats+1).join(str);
}

//for zero-padding, or making sure all values are the same length
function charPadding(theValue, totalNoOfChars, padChar, padAfter, posAndNeg) {
	if (posAndNeg === undefined){
		posAndNeg = false;
	}

	if (padAfter === undefined) {
		//har vi inte angett så "paddar" den före
		padAfter = false;
	}
	if (padChar === undefined) {
		//paddar default med nollor
		padChar = '0';
	}
	if (posAndNeg && theValue >= 0){
		var str = '+' + theValue.toString();
	}else{
		var str = theValue.toString();
	}

	var paddedString = str;

	if (str.length < totalNoOfChars) {
		var padString = generateString(padChar, totalNoOfChars);
		if (padAfter) {
			paddedString = (str+padString).slice(0, totalNoOfChars);
		}else{
			paddedString = (padString+str).slice(-totalNoOfChars);
		}
	}else if (str.length > totalNoOfChars) {
		paddedString = (str).slice(0, totalNoOfChars);
	}
	return paddedString;
}

/**
 * 	decimal funktioner
 */
function roundToNoOfDecimals(floatVal, noOfDecimals) {

	var floatAsString = floatVal.toFixed(noOfDecimals);

	var asFloat = parseFloat(floatAsString);

	// logg('roundToNoOfDecimals('+floatVal+', '+noOfDecimals+'), asFloat = '+asFloat);
	return asFloat;
}

function extractDecimalPartOfFloat(floatVal){
	if (floatVal >= 0){
		var int_val = Math.floor(floatVal);
	}else{
		var int_val = Math.ceil(floatVal);
	}

	var dec_val = floatVal - int_val;

	return dec_val;
}

/**
 * 	matematik funktioner
 */
function pythagoras(a, b) {
	var c = Math.sqrt((a*a) + (b*b));

	return c;
}


/**
 * 	GPS-funktioner
 */
function getDistance(userPos_lat, userPos_lon, dest_lat, dest_lon) {
	var R = 6371;// jordens radie km
	var dLat = (dest_lat - userPos_lat) * (Math.PI / 180);
	//gör om till radianer
	var dLon = (dest_lon - userPos_lon) * (Math.PI / 180);
	userPos_lat = userPos_lat * (Math.PI / 180);
	dest_lat = dest_lat * (Math.PI / 180);

	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(userPos_lat) * Math.cos(dest_lat);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	return R * c;
}

function calculateHeadingFromGpsCoords(fromPos, toPos, inDegrees) {
	if (inDegrees === undefined){
		inDegrees = true;
	}
	var lat1 = fromPos.latitude,
		lon1 = fromPos.longitude,
		lat2 = toPos.latitude,
		lon2 = toPos.longitude;

	var headingRad = Math.atan2(Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1), Math.sin(lon2 - lon1) * Math.cos(lat2));
	var headingDegrees = convertRadToDeg(headingRad);

	if (inDegrees){
		return headingDegrees;
	}else{
		return headingRad;
	}
}

function calculateLatLonfromPixels(mapview, xPixels, yPixels) {

    var region = mapview.getRegion();
    var widthInPixels = mapview.rect.width;
    var heightInPixels = mapview.rect.height;

    // should invert because of the pixel reference frame
    heightDegPerPixel = -region.latitudeDelta / heightInPixels;
    widthDegPerPixel = region.longitudeDelta / widthInPixels;

    return {
        latitude : (yPixels - heightInPixels / 2) * heightDegPerPixel + region.latitude,
        longitude : (xPixels - widthInPixels / 2) * widthDegPerPixel + region.longitude
    };
}

function calcGpsDeltaValues(fromPos, myPos) {

	delta_lat = 0;
	delta_lon = 0;
	distance = 0;

	Ti.API.info('calcGpsDeltaValues(), fromPos.lat = '+fromPos.lat+', fromPos.lon = '+fromPos.lon);
	Ti.API.info('calcGpsDeltaValues(), myPos.lat = '+myPos.lat+', myPos.lon = '+myPos.lon);

	if (fromPos.lat > myPos.lat) {
		delta_lat = fromPos.lat - myPos.lat;
	} else {
		delta_lat = myPos.lat - fromPos.lat;
	}

	if (fromPos.lon > myPos.lon) {
		delta_lon = Math.abs(fromPos.lon - myPos.lon);
	} else {
		delta_lon = Math.abs(myPos.lon - fromPos.lon);
	}

	distance = delta_lat + delta_lon;

	logg('distance between pos = ' + distance + ' deltas, lat = ' + delta_lat + ' lon = ' + delta_lon);

	return {
		distance: distance,
		delta_lat: delta_lat,
		delta_lon: delta_lon
	};
}

function calcCenterPointOfGpsCoords(gpsCoordsArray) {

	var total = gpsCoordsArray.length;
	var X = 0;
	var Y = 0;
	var Z = 0;

	for (var i = 0; i < total; i++) {
		var lat = gpsCoordsArray[i].lat * Math.PI / 180;
		var lon = gpsCoordsArray[i].lon * Math.PI / 180;

		var x2 = Math.cos(lat) * Math.cos(lon);
		var y2 = Math.cos(lat) * Math.sin(lon);
		var z2 = Math.sin(lat);

		X += x2;
		Y += y2;
		Z += z2;
	}

	X = X / total;
	Y = Y / total;
	Z = Z / total;

	var newLon = Math.atan2(Y, X);
	var Hyp = Math.sqrt(X * X + Y * Y);
	var newLat = Math.atan2(Z, Hyp);

	return {
		lat: newLat * 180 / Math.PI,
		lon: newLon * 180 / Math.PI
	};
}


/**
 * 	array funktioner
 */
function findMaxInArray(array) {
	var max = 0;
	var a = array.length;
	for (var counter = 0; counter < a; counter++) {
		if (array[counter] > max) {
			max = array[counter];
		}
	}
	return max;
}



/**
  **
   ** 	PULL TO REFRESH för tableView:s
  **
 */
var ptrStatusLabel,
	ptrActivityIndicator,
	ptrArrowView,
	reloading = false,
	pulling = false,
	pullHeight = OS_is_iOS7Plus ? -40 : -50;//48;//-65;

/* texter */
var pullTxt = L('pullToRefreshPullDown', 'Dra för att uppdatera...'),
	releaseTxt =  L('pullToRefreshRelease', 'Släpp för att uppdatera...'),
	lastUpdatedTxt = L('pullToRefreshLastUpdated', 'Senast uppdaterad:'),
	updatingTxt = L('pullToRefreshReloading', 'Uppdaterar...');


function addPullToRefresh(forTableView) {

	var tableHeader = Ti.UI.createView({
		backgroundColor: "#e0e0e0",
		width: '100%',
		height: dp(54)
	});

	ptrArrowView = Ti.UI.createView({
		backgroundImage: 'images/arrow.png',
		width: dp(22),
		height: dp(22),
		bottom: dp(0),
		left: dp(52)
	});

	ptrStatusLabel = Ti.UI.createLabel({
		text: pullTxt,
		bottom: dp(2.5),
		color: blackFontColor,
		textAlign: 'center',
		font: fontBold12,
	});

	// ptrLastUpdatedLabel = Ti.UI.createLabel({
		// text: lastUpdatedTxt + ' ' + formatDate(),
		// left: dp(45),
		// right: dp(45),
		// bottom: dp(0),//15
		// color: '#444',
		// textAlign: 'center',
		// font: font11,
	// });
/*
	ptrActivityIndicator = Titanium.UI.createActivityIndicator({
		left: dp(88),
		bottom: dp(0),//13
		width: dp(22),
		height: dp(22),
		style: Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
	});
*/
	tableHeader.add(ptrArrowView);
	tableHeader.add(ptrStatusLabel);
	// tableHeader.add(ptrLastUpdatedLabel);
	// tableHeader.add(ptrActivityIndicator);

	forTableView.headerPullView = tableHeader;

	pulling = false;
	reloading = false;

	function beginReloading() {
		//skickar ett event till tableView:n
		forTableView.fireEvent('tableViewWasPulledToRefresh');
		endReloading(forTableView);
	}


	forTableView.addEventListener('scroll', function(e) {
		var offset = e.contentOffset.y;
		if (offset <= pullHeight && !pulling && !reloading) {
			var t = Ti.UI.create2DMatrix();
			t = t.rotate(-180);
			pulling = true;
			ptrArrowView.animate({
				transform: t,
				duration: 180
			});
			ptrStatusLabel.text = releaseTxt;
		} else if (pulling && (offset > pullHeight && offset < 0) && !reloading) {
			pulling = false;
			var t = Ti.UI.create2DMatrix();
			ptrArrowView.animate({
				transform: t,
				duration: 180
			});
			ptrStatusLabel.text = pullTxt;
		}
	});

	forTableView.addEventListener('dragend', function(e) {
		if (pulling && !reloading) {
			reloading = true;
			pulling = false;
			// ptrArrowView.hide();
			// ptrActivityIndicator.show();
			// ptrStatusLabel.text = updatingTxt;
			// forTableView.setContentInsets({ top: Math.abs(pullHeight) }, { animated: true });
			// setTimeout(function(){
			// }, 350);
			beginReloading();
		}
	});
}

function endReloading(forTableView) {
	if (OS_is_Android){
		return;
	}
	setTimeout(function(e){
		// when you're done, just reset
		forTableView.setContentInsets({ top: 0 }, { animated: true });
		reloading = false;
		// ptrLastUpdatedLabel.text = lastUpdatedTxt + ' ' + formatDate();
		ptrStatusLabel.text = pullTxt;
		// ptrActivityIndicator.hide();
		ptrArrowView.transform = Ti.UI.create2DMatrix();
		// ptrArrowView.show();
	}, 350);
}
/**  END >> pull to refresh **/



/**
 * 	datum funktioner
 */
function formatDate() {
	var date = new Date();
	// var datestr = date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
	// if (date.getHours() >= 12) {
		// datestr += ' ' + (date.getHours() == 12 ? date.getHours() : date.getHours() - 12) + ':' + date.getMinutes() + ' PM';
	// } else {
		// datestr += ' ' + date.getHours() + ':' + date.getMinutes() + ' AM';
	// }
	return String.formatDate(date, 'short')+'  '+String.formatTime(date, 'short');
}


/**
 * 	aktiverar tryckkänsla på ett valfritt Ti.UI-objekt som stödjer eventen 'touch...' & property:n opacity
 */
function addTouchyFeeling(theObject) {
	theObject.addEventListener('touchstart', function(e) {
		theObject.opacity = 0.5;
	});
	theObject.addEventListener('touchend', function(e) {
		if (OS_is_iOS) {
			theObject.touchEnabled = false;
		}
		setTimeout(function(e) {
			if (OS_is_Android){
				theObject.touchEnabled = false;
			}
			theObject.opacity = 1.0;
		}, 180);
		var _object = theObject;
		setTimeout(function(e) {
			// logg('addTouchyFeeling, _object = '+_object);
			_object.touchEnabled = true;
		}, 1200);
	});
	theObject.addEventListener('touchcancel', function(e) {
		setTimeout(function(e) {
			theObject.opacity = 1.0;
		}, 180);
	});
}


/**
 * 	egen mer avancerad "logg" funktion istället för: Ti.API.info('...')
 */
function logg(msg, object) {

	//kollar om vi är i "debug"-mode
	if (!debugMode) {
		return;
	}

	if (typeof msg == 'object' && object === undefined) {
		Ti.API.debug('logg() has detected "msg" is [object], try to stringify "msg"...');
		object = msg;
		msg = '';
	}
	var objStr = '';
	if (object !== undefined){
		try {
			objStr = ' '+JSON.stringify(object);
		} catch(err) {
			Ti.API.warn('## catch-ERROR in function logg(): UNABLE TO STRINGIFY "object".');
		}
	}

	if (win !== null) {
		if (win !== undefined) {
			if (win.url !== undefined) {
				Ti.API.info(win.url + ' >> ' + msg+objStr);
			}else{
				Ti.API.info(' >> ' + msg+objStr);
			}
		} else {
			Ti.API.info('app.js >> ' + msg+objStr);
		}
	} else {
		Ti.API.info('win = ??? >> ' + msg+objStr);
	}
}


/**
 * 	funktion för att sätta "developer"-färger på UI-objekt
 */
function devColor(theObject, theColor, theOpacity) {
	if (!debugMode) {
		logg('devColor() debugMode = false');
		return;
	}
	if (theOpacity === undefined) {
		// logg('devColor() theOpacity is undefined, setting it to 0.5...');
		theOpacity = 0.5;
	}
	if (theColor === undefined) {
		// logg('devColor() theColor is undefined, setting it to random...');
		theColor = 'random';
	}
	if (theColor == 'random') {
		// logg('devColor() theColor is random');
		theColor = Math.floor(Math.random() * 16777215).toString(16);
		if (theColor.length < 6) {
			theColor = '0' + theColor;
		}
		theColor = '#' + theColor;
	}
	// logg('theColor = ' + theColor);
	theObject.backgroundColor = theColor;
	theObject.opacity = theOpacity;
}


/**
 *	egen funktion för att visa en alertDialog med titeln Information
 */
function alertD(msg) {
	var dialog = Ti.UI.createAlertDialog({
		message: msg,
		ok: 'OK',
		title: 'Information'
	}).show();
}


/**
 * 	mer avancerad funktion för att visa en egen alertDialog
 * 		man kan ange titel, meddelande och vilka knappar man vill ha (eller bara ett meddelande)
 */
function alertD_reply(title, msg, btns) {

	if (title == '') {
		title = 'Information';
	}
	if (btns.length == 0) {
		btns.push('OK');
	}

	var dialog = Ti.UI.createAlertDialog({
		message: msg,
		buttonNames: btns,
		title: title
	});

	//vi får tillbaka en alertDailog så vi kan koppla eventListerner till den, så vi vet vilken knapp som tryckts på
	return dialog;
}



/**
 *	funktion för att räkna om dp till faktiska pixlar (för enklare kodning för både iOS & Android)
 */
function pc(percent, inRelationToObject) {
	var screenW = Ti.Platform.displayCaps.platformWidth;
	var screenH = Ti.Platform.displayCaps.platformHeight;
	if (inRelationToObject !== undefined){
		//Nu kan man räkna i procent till andra objekts bredd eller höjd
		screenW = inRelationToObject.width;
		screenH = inRelationToObject.height;
	}

	var percentSign = percent.search('%');
	if (percentSign > -1){
		percent = percent.replace('%', '');
	}
	percent = parseFloat(percent) / 100;
	logg('pc(), percent = '+percent+', screenW*percent = '+screenW*percent+', screenH*percent = '+screenH*percent);

	return { hori: Math.round(screenW * percent), vert: Math.round(screenH * percent) };
}

function dp(DPUnits) {
	if (DPUnits == null) {
		return null;
	}
	if (DPUnits.toString().search('%') >= 0) {
		return DPUnits;
	}

	if (DPUnits.toString().indexOf("dp") > 0) {
		DPUnits = getValueFromDPUnits(DPUnits);
	};
	if (OS_is_Android) {
		Pixels = (DPUnits * (Titanium.Platform.displayCaps.dpi / 160));
		// logg("DPUnitsToPixels (Android)>> '"+DPUnits+"dp' = "+Pixels+"px");
	} else {
		Pixels = DPUnits;
		// logg("DPUnitsToPixels (iOS) >> '"+DPUnits+"dp' = "+Pixels+"px");
	};
	return Pixels;
}

/** OBS! används av funktionen dp() **/
//funktion som gör att man kan räkna med värden angivna i "dp" (även i string med formattering, ex: "10dp")
//	konverterar till integer: px
function getValueFromDPUnits(dp_value) {
	dp_value.slice(1, dp_value.length - 3);
	// logg('getValueFromDPUnits >> '+parseInt(dp_value));
	return parseInt(dp_value);
}


/**
 * 	lägger till en vertikal linje, ange top och bottom värde eller height som en Dictionary och ev en färg
 */
function addVerticalDividerLine(data) {
	if (data.color === undefined) {
		data.color = '#777';
	}
	return Ti.UI.createView({
		width: dp(0.5),
		height: dp(data.height),
		top: dp(data.top),
		left: dp(data.left),
		right: dp(data.right),
		bottom: dp(data.bottom),
		backgroundColor: data.color,
		touchEnabled: false
	});
}


/**
 * 	lägger till en horisontell linje, ange ett top-värde ELLER ett bottom värde för plascering
 */
function addDividerLine(pTop, pBottom, pColor) {
	if (pColor === undefined) {
		pColor = dividerLineColor;
	}
	return Ti.UI.createView({
		width: '100%',
		height: dp(0.5),
		top: dp(pTop),
		bottom: dp(pBottom),
		backgroundColor: pColor,
		touchEnabled: false
	});
}


/**
 * 	lägger till en tomt utrymme, bra för att fylla ut ifall man använder property:n -> layout: 'vertical'
 */
function addSomeSpace(vHeight) {
	return Ti.UI.createView({
		height: dp(vHeight),
		width: '100%',
		touchEnabled: false
	});
}


/** färgfunktioner **/

var noOfColors = 0;

var theColors = [];
var colorsOnOneRow = 0;
var noOfColorRows = 0;
var coloredViews = [];

function generateColors(){
	theColors = [];
	colorsOnOneRow = 0;
	noOfColorRows = 0;

	var hueStep = 30;//Math.round(hueStepSlider.value),
		satStep = 22;//Math.round(saturationStepSlider.value),
		valStep = 20;//Math.round(valueStepSlider.value);

	for (var hue = 0; hue <= 360-hueStep; hue += hueStep){ // += 30
		noOfColorRows++;
		for (var sat = 35; sat <= 100; sat += satStep){ // += 22
			for (var val = sat-15; val <= 100; val += valStep){ // += 20
				if (hue == 0){
					colorsOnOneRow++;
				}
				theColors.push(hsvToHex(hue, sat, val));
			}
		}
	}

	noOfColorRows++;
	grayStep = Math.round(100 / colorsOnOneRow);
	for (var val = 0; val <= 100; val += grayStep) { // += 20
		// colorsOnOneRow++;
		theColors.push(hsvToHex(0, 0, val));
	}


	Ti.API.info('theColors = '+theColors);
	Ti.API.info('theColors.length = '+theColors.length);

	// numberOfColorsLbl.text = '# of colors: '+theColors.length+'  |  rows: '+noOfColorRows+ '  |  columns: '+colorsOnOneRow;
	// makeColoredViews();
}

function hsvToHex(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (h && s === undefined && v === undefined) {
        s = h.s, v = h.v, h = h.h;
    }
    h = h/360;
    s = s/100;
    v = v/100;

    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    // Ti.API.info('r = '+r+', g = '+g+', b = '+b);

    return rgbToHex(Math.floor(r * 255), Math.floor(g * 255), Math.floor(b * 255));
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}


/**
 * animerar från en färg till en annan
 *
 * START: animateColor dependencies
**/
function valueIsWithinRange(theValue, minVal, maxVal) {
	if (theValue >= minVal && theValue <= maxVal){
		return true;
	}else{
		return false;
	}
}

function extractColorValuesFromHex(hexColor) {
	var r, g, b;

	if (hexColor.length == 4){
		hex_len = 1;
	}else{
		hex_len = 2;
	}

	r = hexColor.substr(0*hex_len+1, hex_len);
	g = hexColor.substr(1*hex_len+1, hex_len);
	b = hexColor.substr(2*hex_len+1, hex_len);

	if (hex_len == 1) {
		//fixar så det blir 2 bytes värden, tex #050 blir #005500 och #F00 blir #FF0000
		r = r+r;
		g = g+g;
		b = b+b;
	}
	logg('extractColorValuesFromHex()  |  r = '+r+', g = '+g+', b = '+b);
	return {
		red: parseInt(r, 16),
		green: parseInt(g, 16),
		blue: parseInt(b, 16)
	};
}

function intToHex(intValue, digits){
	if (digits === undefined){
		digits = 2;
	}
	var hex = intValue.toString(16);
	// logg('intToHex, intValue = '+intValue+' hex (before): '+hex);
	if (hex.length < digits) {
		hex = generateString('0', digits - hex.length) + hex;
	}
	// logg('intToHex, hex (after) = '+hex);
	return hex;
}

function checkHexColorRange(intValue) {
	if (intValue > 255){
		intValue = 255;
	}
	if (intValue < 0){
		intValue = 0;
	}
	return intValue;
}
/** SLUT: animateColor dependencies **/



/**
 * 	konverterar radianer till grader
 */
function convertRadToDeg(radians) {
	var deg = (radians) * (180/Math.PI);
	if (deg < 0) {
		deg = 360 + deg;
	}
	return deg;
}

/**
 * 	konverterar grader till radianer
 */
function convertDegToRad(degrees){
	return (degrees) * (Math.PI/180);
}



/** NYTT 2015-03-02 **/
//
// ** custom scrollableView paging control **
//
function makeDots(viewCount, activeColor, defColor) {
	var sizeOfDot = 6, //storlek på prickar
		dotSpacing = 6; //avstånd mellan prickar

	var pageIndicatorViewHolder = Ti.UI.createView({
		height: dp(sizeOfDot * 2),
		width: dp(viewCount * (sizeOfDot + dotSpacing) + dotSpacing),
		bottom: dp(10),
		layout: 'horizontal',
		touchEnabled: false,
	});

	dotArray = [];
	for (var i = 0; i < viewCount; i++) {
		dotArray[i] = Ti.UI.createView({
			//markerar första pricken som "aktiv" = lila
			backgroundColor: i == 0 ? activeColor : defColor,
			height: dp(sizeOfDot),
			width: dp(sizeOfDot),
			left: dp(dotSpacing),
			top: dp(sizeOfDot / 2),
			borderRadius: dp(sizeOfDot / 2),
			touchEnabled: false,
			activeColor: activeColor,
			defColor: defColor
		});
		pageIndicatorViewHolder.add(dotArray[i]);
	}

	return pageIndicatorViewHolder;
}

//
// uppdaterar custom paging control
function updateDots(cPage) {
	//nollställer föregående prick
	for (var i = 0; i < dotArray.length; i++) {
		if (cPage !== i) {
			dotArray[i].backgroundColor = dotArray[i].defColor;
		}
	}
	//markerar prick som aktiv
	dotArray[cPage].backgroundColor = dotArray[cPage].activeColor;
}


//
// "kastar runt" (slumpar) posterna från en array och returnerar en ny array
// 		dvs den urspungliga arrayen påverkas inte utan bibehåller ordningen
function randomizeArray(originalArray) {
	//
	//intern funktion för att "kasta-runt" posterna
	// OBS! shuffle kan inte ropas på utanför randomizeArray() funktionen
	function shuffle(array) {
		var counter = array.length,
			temp,
			index;

		// while there are elements in the array
		while (counter > 0) {
			// pick a random index
			index = Math.floor(Math.random() * counter);

			// decrease counter by 1
			counter--;

			// and swap the last element with it
			temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	}

	//gör en kopia av originalArray (som skickades in till funktionen)
	var newArray = originalArray.slice(0);

	//skumpar alla poster i newArray och returnerar den
	return shuffle(newArray);
}



/** #################### **/
/** ####### SLUT ####### **/
/** #################### **/
